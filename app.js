"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mist_tools_ts_1 = require("@mist-cloud-eu/mist-tools-ts");
const PRODUCTS_1 = require("./PRODUCTS");
const USERS_1 = require("./USERS");
const STOCK_1 = require("./STOCK");
(0, mist_tools_ts_1.mistService)({
    "product-service": (envelope) => {
        let product = PRODUCTS_1.PRODUCTS.find((x) => x.id === envelope.payload.productId);
        (0, mist_tools_ts_1.postToRapid)("reply", product);
    },
    "user-service": (envelope) => {
        let user = USERS_1.USERS.find((x) => x.id === envelope.payload.userId);
        (0, mist_tools_ts_1.postToRapid)("user-service-reply", {
            locationId: user === null || user === void 0 ? void 0 : user.preferredLocation,
            productId: envelope.payload.productId
        });
    },
    "stock-service": (envelope) => {
        let product = PRODUCTS_1.PRODUCTS.find((x) => x.id === envelope.payload.productId);
        let stock = STOCK_1.STOCK.find((x) => x.product === (product === null || product === void 0 ? void 0 : product.id));
        (0, mist_tools_ts_1.postToRapid)("reply", stock);
    }
});
