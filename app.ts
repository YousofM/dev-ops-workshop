import {
  mistService,
  Envelope,
  postToRapid,
} from "@mist-cloud-eu/mist-tools-ts";
import { PRODUCTS } from "./PRODUCTS";
import { USERS } from "./USERS";
import { STOCK } from "./STOCK";

mistService({
  "product-service": (envelope: Envelope<{ productId: string, userId: string }>) => {
    let product = PRODUCTS.find((x) => x.id === envelope.payload.productId);
    postToRapid("reply", product);
  },
  "user-service": (envelope: Envelope<{ productId: string, userId: string }>) => {
    let user = USERS.find((x) => x.id === envelope.payload.userId);
    postToRapid("user-service-reply", {
      locationId: user?.preferredLocation,
      productId: envelope.payload.productId
    });
  },
  "stock-service": (envelope: Envelope<{ productId: string, userId: string }>) => {
    let product = PRODUCTS.find((x) => x.id === envelope.payload.productId);
    let stock = STOCK.find((x) => x.product === product?.id);
    postToRapid("reply", stock);
  }
});
